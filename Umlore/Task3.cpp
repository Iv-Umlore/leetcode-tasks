#include <iostream>
#include <string>
#include <set>
#include <map>

// TASK #3
// https://leetcode.com/problems/longest-substring-without-repeating-characters/description/
// Кратко: Дана строка s - необходимо найти длинну самой длинной последновательности символов без повторов

class Solution {
public:
    int lengthOfLongestSubstring(std::string s) {
        std::set<char> deletedSet = std::set<char>();

        std::map<char, int> lengthByLetter = std::map<char, int>();
        int maxCounter = 0;

        for (int i = 0; i < s.size(); i++) {
            if (lengthByLetter.find(s[i]) == lengthByLetter.end()) {
                lengthByLetter.insert(std::pair<char, int>(s[i], 0));
            }
            else {
                if (maxCounter < lengthByLetter[s[i]])
                    maxCounter = lengthByLetter[s[i]];

                for (auto letter = lengthByLetter.begin(); letter != lengthByLetter.end(); letter++) {
                    if (lengthByLetter[s[i]] < letter->second) {
                        if (maxCounter < letter->second)
                            maxCounter = letter->second;

                        deletedSet.insert(letter->first);
                    }
                }

                for (auto iter = deletedSet.begin(); iter != deletedSet.end(); iter++)
                    lengthByLetter.erase(*iter);

                deletedSet.clear();
                lengthByLetter[s[i]] = 0;
            }

            for (auto letter = lengthByLetter.begin(); letter != lengthByLetter.end(); letter++)
            {
                letter->second++;
            }
        }

        for (auto letter = lengthByLetter.begin(); letter != lengthByLetter.end(); letter++)
        {
            if (maxCounter < letter->second)
                maxCounter = letter->second;
        }

        return maxCounter;
    }
};


int main() {

    Solution sol = Solution();

    std::string s = "abcadc";

    int res = sol.lengthOfLongestSubstring(s);

    std::cout << res;
}
